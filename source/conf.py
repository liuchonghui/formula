# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
project = 'formula'
copyright = '2024, liuchonghui'
author = 'liuchonghui'
release = '1.0'

import sphinx_rtd_theme
import recommonmark
from recommonmark.transform import AutoStructify

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx_rtd_theme",
    "recommonmark",
    "sphinx_markdown_tables",
    "sphinx.ext.autosectionlabel"
]

source_suffix = ['.rst', '.md', '.txt']

templates_path = ['_templates']

exclude_patterns = []

master_doc = 'index'

language = 'zh_CN'

pygments_style = None

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

htmlhelp_basename = 'formuladoc'

gitlab_source_root = 'https://gitlab.com/liuchonghui/formula/-/raw/main/source/'

def setup(app):
    app.add_config_value('recommonmark_config', {
        'auto_toc_maxdepth': 1,
        #'url_resolver': lambda url: gitlab_source_root + url,
        'enable_auto_toc_tree': True,
        'auto_toc_tree_section': 'Links',
        'enable_math': True,
        'enable_inline_math': True,
        'enable_eval_rst': True,
    }, True)
    app.add_transform(AutoStructify)